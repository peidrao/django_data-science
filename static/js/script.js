$(document).ready(function () {
  $(".ui.dropdown").dropdown();

  $(".message .close").on("click", function () {
    $(this).closest(".message").transition("fade");
  });

  $(".button-btn").on("click", function () {
    $(".ui.modal").modal({blurring: true}).modal("show");
  });
});
