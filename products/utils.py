import base64
import seaborn as sns
import matplotlib.pyplot as plt
from django.contrib.auth.models import User


from io import BytesIO


def get_salesman_by_id(salesman_id):
    salesman = User.objects.get(id=salesman_id)
    return salesman


def get_image():
    buffer = BytesIO()
    plt.savefig(buffer, format='png')

    buffer.seek(0)
    image_png = buffer.getvalue()

    graph = base64.b64encode(image_png)
    graph = graph.decode('utf-8')
    buffer.close()

    return graph


def get_simple_plot(chart_type, *args, **kwargs):
    plt.switch_backend('AGG')
    fig = plt.figure(figsize=(10, 4))
    x = kwargs.get('x')
    y = kwargs.get('y')
    data = kwargs.get('data')
    if chart_type == 'bar plot':
        title = "total price by day (bar)"
        plt.title(title)
        plt.bar(x=x, height=y)
    elif chart_type == 'line plot':
        title = "total price by day (line)"
        plt.title(title)
        plt.plot(x, y)
    else:
        title = "Product count"
        plt.title(title)
        sns.countplot('name', data=data)
    plt.xticks(rotation=45)
    plt.tight_layout()

    graph = get_image()
    return graph
