from django.urls import path

from . import views


app_name='product'

urlpatterns = [
    path('', views.index, name='home'),
    path('add', views.add_purchase_view, name='add'),
    path('sales', views.sales_dist_view, name='sales')
]
