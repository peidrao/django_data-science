from django.urls import path

from . import views

app_name='customer'

urlpatterns = [
    path('customer', views.customer_corr_view, name='customer-view'),
]
