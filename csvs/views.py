import csv
from django.shortcuts import render
from django.contrib.auth.models import User

from .forms import CsvForm
from .models import Csv
from products.models import Purchase, Product

# Create your views here.


def upload_file(request):
    message = None
    error_message = None
    success_message = None
    if request.method == 'POST':
        form = CsvForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            form.save()
            try:
                
                file = Csv.objects.get(activated=False)

                with open(file.file_name.path, 'r') as f:
                    reader = csv.reader(f)

                    for row in reader:
                        row = ''.join(row)
                        row = row.replace(';', ' ')
                        row = row.split()
                        user = User.objects.get(id=row[3])
                        prod, _ = Product.objects.get_or_create(name=row[0])
                        
                        Purchase.objects.create(
                            product=prod,
                            price=int(row[2]),
                            quantity=int(row[1]),
                            salesman=user,
                            date=row[4] + ' ' + row[5])

                file.activated = True
                file.save()
                success_message = 'Upload sucessfully!'
            except Exception as exce:
                error_message = 'Ops. Something went wrong...'

    context = {
        'error_message': error_message,
        'success_message': success_message,
        'form': CsvForm(),
    }

    return render(request, 'upload_file.html', context)
