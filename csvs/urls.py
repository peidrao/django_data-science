from django.urls import path

from . import views

app_name='csv'

urlpatterns = [
    path('upload', views.upload_file, name='upload_file'),
]
